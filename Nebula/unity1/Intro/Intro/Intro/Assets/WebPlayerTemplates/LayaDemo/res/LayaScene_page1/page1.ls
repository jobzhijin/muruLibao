{
	"type":"Scene",
	"props":{
		"name":"page1",
		"enableFog":false,
		"fogStart":0,
		"fogRange":300
	},
	"customProps":{
		"skyBox":{},
		"lightmaps":[],
		"ambientColor":[
			0.212,
			0.227,
			0.259
		],
		"fogColor":[
			0.5,
			0.5,
			0.5
		]
	},
	"child":[
		{
			"type":"Camera",
			"props":{
				"isStatic":false,
				"name":"Main Camera",
				"clearFlag":0,
				"orthographic":false,
				"fieldOfView":60,
				"nearPlane":0.3,
				"farPlane":1000
			},
			"customProps":{
				"layer":0,
				"translate":[
					0,
					1,
					-22.58
				],
				"rotation":[
					0,
					1,
					0,
					0
				],
				"scale":[
					1,
					1,
					1
				],
				"viewport":[
					0,
					0,
					1,
					1
				],
				"clearColor":[
					0.4745098,
					0.4745098,
					0.4745098,
					0
				]
			},
			"components":{},
			"child":[]
		},
		{
			"type":"Sprite3D",
			"props":{
				"isStatic":false,
				"name":"奶滴obj"
			},
			"customProps":{
				"layer":0,
				"translate":[
					0,
					0,
					0
				],
				"rotation":[
					0,
					0,
					0,
					-1
				],
				"scale":[
					0.01,
					0.01,
					0.01
				]
			},
			"components":{},
			"child":[
				{
					"type":"MeshSprite3D",
					"props":{
						"isStatic":false,
						"name":"default"
					},
					"customProps":{
						"layer":0,
						"translate":[
							0,
							0,
							0
						],
						"rotation":[
							0,
							0,
							0,
							-1
						],
						"scale":[
							1,
							1,
							1
						],
						"meshPath":"Assets/奶滴obj-default.lm",
						"materials":[
							{
								"type":"Laya.StandardMaterial",
								"path":"Assets/nai.lmat"
							}
						]
					},
					"components":{},
					"child":[]
				}
			]
		},
		{
			"type":"ShuriKenParticle3D",
			"props":{
				"isStatic":false,
				"name":"Particle System (1)"
			},
			"customProps":{
				"layer":0,
				"translate":[
					0,
					0,
					0
				],
				"rotation":[
					0.7071068,
					0,
					0,
					-0.7071068
				],
				"scale":[
					1,
					1,
					1
				],
				"isPerformanceMode":true,
				"duration":2,
				"looping":true,
				"prewarm":false,
				"startDelayType":0,
				"startDelay":0,
				"startDelayMin":0,
				"startDelayMax":0,
				"startLifetimeType":0,
				"startLifetimeConstant":2,
				"startLifetimeConstantMin":0,
				"startLifetimeConstantMax":2,
				"startLifetimeGradient":{
					"startLifetimes":[]
				},
				"startLifetimeGradientMin":{
					"startLifetimes":[]
				},
				"startLifetimeGradientMax":{
					"startLifetimes":[]
				},
				"startSpeedType":0,
				"startSpeedConstant":0,
				"startSpeedConstantMin":0,
				"startSpeedConstantMax":0,
				"threeDStartSize":false,
				"startSizeType":0,
				"startSizeConstant":16.6,
				"startSizeConstantMin":0,
				"startSizeConstantMax":16.6,
				"startSizeConstantSeparate":[
					16.6,
					1,
					1
				],
				"startSizeConstantMinSeparate":[
					0,
					0,
					0
				],
				"startSizeConstantMaxSeparate":[
					16.6,
					1,
					1
				],
				"threeDStartRotation":false,
				"startRotationType":0,
				"startRotationConstant":0,
				"startRotationConstantMin":0,
				"startRotationConstantMax":0,
				"startRotationConstantSeparate":[
					0,
					0,
					0
				],
				"startRotationConstantMinSeparate":[
					0,
					0,
					0
				],
				"startRotationConstantMaxSeparate":[
					0,
					0,
					0
				],
				"randomizeRotationDirection":0,
				"startColorType":0,
				"startColorConstant":[
					1,
					1,
					1,
					1
				],
				"startColorConstantMin":[
					0,
					0,
					0,
					0
				],
				"startColorConstantMax":[
					1,
					1,
					1,
					1
				],
				"gravity":[
					0,
					-9.81,
					0
				],
				"gravityModifier":0,
				"simulationSpace":1,
				"scaleMode":1,
				"playOnAwake":true,
				"maxParticles":1000,
				"autoRandomSeed":true,
				"randomSeed":1867880920,
				"emission":{
					"enable":true,
					"emissionRate":1,
					"emissionRateTip":"Time",
					"bursts":[
						{
							"time":0,
							"min":0,
							"max":0
						}
					]
				},
				"shape":{
					"enable":true,
					"shapeType":0,
					"sphereRadius":0.01,
					"sphereEmitFromShell":false,
					"sphereRandomDirection":0,
					"hemiSphereRadius":0.01,
					"hemiSphereEmitFromShell":false,
					"hemiSphereRandomDirection":0,
					"coneAngle":25,
					"coneRadius":0.01,
					"coneLength":5,
					"coneEmitType":0,
					"coneRandomDirection":0,
					"boxX":1,
					"boxY":1,
					"boxZ":1,
					"boxRandomDirection":0,
					"circleRadius":0.01,
					"circleArc":360,
					"circleEmitFromEdge":false,
					"circleRandomDirection":0
				},
				"colorOverLifetime":{
					"enable":true,
					"color":{
						"type":1,
						"constant":[
							0,
							0,
							0,
							0
						],
						"gradient":{
							"alphas":[
								{
									"key":0,
									"value":0
								},
								{
									"key":0.4970626,
									"value":0.6666667
								},
								{
									"key":1,
									"value":0
								}
							],
							"rgbs":[
								{
									"key":0,
									"value":[
										1,
										1,
										1
									]
								},
								{
									"key":1,
									"value":[
										1,
										1,
										1
									]
								}
							]
						},
						"constantMin":[
							0,
							0,
							0,
							0
						],
						"constantMax":[
							0,
							0,
							0,
							0
						],
						"gradientMax":{
							"alphas":[
								{
									"key":0,
									"value":0
								},
								{
									"key":0.4970626,
									"value":0.6666667
								},
								{
									"key":1,
									"value":0
								}
							],
							"rgbs":[
								{
									"key":0,
									"value":[
										1,
										1,
										1
									]
								},
								{
									"key":1,
									"value":[
										1,
										1,
										1
									]
								}
							]
						}
					}
				},
				"renderMode":0,
				"stretchedBillboardCameraSpeedScale":0,
				"stretchedBillboardSpeedScale":0,
				"stretchedBillboardLengthScale":2,
				"sortingFudge":0,
				"material":{
					"type":"Laya.ShurikenParticleMaterial",
					"path":"Assets/tietu/gangdi.lmat"
				}
			},
			"components":{},
			"child":[
				{
					"type":"ShuriKenParticle3D",
					"props":{
						"isStatic":false,
						"name":"Particle System"
					},
					"customProps":{
						"layer":0,
						"translate":[
							0,
							0,
							0
						],
						"rotation":[
							0,
							0,
							0,
							-1
						],
						"scale":[
							1,
							1,
							1
						],
						"isPerformanceMode":true,
						"duration":5,
						"looping":true,
						"prewarm":false,
						"startDelayType":0,
						"startDelay":0,
						"startDelayMin":0,
						"startDelayMax":0,
						"startLifetimeType":0,
						"startLifetimeConstant":2,
						"startLifetimeConstantMin":0,
						"startLifetimeConstantMax":2,
						"startLifetimeGradient":{
							"startLifetimes":[]
						},
						"startLifetimeGradientMin":{
							"startLifetimes":[]
						},
						"startLifetimeGradientMax":{
							"startLifetimes":[]
						},
						"startSpeedType":0,
						"startSpeedConstant":0,
						"startSpeedConstantMin":0,
						"startSpeedConstantMax":0,
						"threeDStartSize":false,
						"startSizeType":0,
						"startSizeConstant":57.9,
						"startSizeConstantMin":0,
						"startSizeConstantMax":57.9,
						"startSizeConstantSeparate":[
							57.9,
							1,
							1
						],
						"startSizeConstantMinSeparate":[
							0,
							0,
							0
						],
						"startSizeConstantMaxSeparate":[
							57.9,
							1,
							1
						],
						"threeDStartRotation":false,
						"startRotationType":0,
						"startRotationConstant":0,
						"startRotationConstantMin":0,
						"startRotationConstantMax":0,
						"startRotationConstantSeparate":[
							0,
							0,
							0
						],
						"startRotationConstantMinSeparate":[
							0,
							0,
							0
						],
						"startRotationConstantMaxSeparate":[
							0,
							0,
							0
						],
						"randomizeRotationDirection":0,
						"startColorType":0,
						"startColorConstant":[
							1,
							1,
							1,
							0.421
						],
						"startColorConstantMin":[
							0,
							0,
							0,
							0
						],
						"startColorConstantMax":[
							1,
							1,
							1,
							0.421
						],
						"gravity":[
							0,
							-9.81,
							0
						],
						"gravityModifier":0,
						"simulationSpace":1,
						"scaleMode":1,
						"playOnAwake":true,
						"maxParticles":1000,
						"autoRandomSeed":true,
						"randomSeed":1530077319,
						"emission":{
							"enable":true,
							"emissionRate":2,
							"emissionRateTip":"Time",
							"bursts":[]
						},
						"shape":{
							"enable":true,
							"shapeType":0,
							"sphereRadius":0.01,
							"sphereEmitFromShell":false,
							"sphereRandomDirection":0,
							"hemiSphereRadius":0.01,
							"hemiSphereEmitFromShell":false,
							"hemiSphereRandomDirection":0,
							"coneAngle":25,
							"coneRadius":0.01,
							"coneLength":5,
							"coneEmitType":0,
							"coneRandomDirection":0,
							"boxX":1,
							"boxY":1,
							"boxZ":1,
							"boxRandomDirection":0,
							"circleRadius":0.01,
							"circleArc":360,
							"circleEmitFromEdge":false,
							"circleRandomDirection":0
						},
						"colorOverLifetime":{
							"enable":true,
							"color":{
								"type":1,
								"constant":[
									0,
									0,
									0,
									0
								],
								"gradient":{
									"alphas":[
										{
											"key":0,
											"value":0
										},
										{
											"key":0.4264744,
											"value":1
										},
										{
											"key":1,
											"value":0
										}
									],
									"rgbs":[
										{
											"key":0,
											"value":[
												1,
												1,
												1
											]
										},
										{
											"key":1,
											"value":[
												1,
												1,
												1
											]
										}
									]
								},
								"constantMin":[
									0,
									0,
									0,
									0
								],
								"constantMax":[
									0,
									0,
									0,
									0
								],
								"gradientMax":{
									"alphas":[
										{
											"key":0,
											"value":0
										},
										{
											"key":0.4264744,
											"value":1
										},
										{
											"key":1,
											"value":0
										}
									],
									"rgbs":[
										{
											"key":0,
											"value":[
												1,
												1,
												1
											]
										},
										{
											"key":1,
											"value":[
												1,
												1,
												1
											]
										}
									]
								}
							}
						},
						"sizeOverLifetime":{
							"enable":true,
							"size":{
								"type":0,
								"separateAxes":false,
								"gradient":{
									"sizes":[
										{
											"key":0,
											"value":0
										},
										{
											"key":1,
											"value":1
										}
									]
								},
								"gradientX":{
									"sizes":[
										{
											"key":0,
											"value":0
										},
										{
											"key":1,
											"value":1
										}
									]
								},
								"gradientY":{
									"sizes":[
										{
											"key":0,
											"value":0
										},
										{
											"key":1,
											"value":1
										}
									]
								},
								"gradientZ":{
									"sizes":[
										{
											"key":0,
											"value":0
										},
										{
											"key":1,
											"value":1
										}
									]
								},
								"constantMin":0,
								"constantMax":0,
								"constantMinSeparate":[
									0,
									0,
									0
								],
								"constantMaxSeparate":[
									0,
									0,
									0
								],
								"gradientMin":{
									"sizes":[]
								},
								"gradientMax":{
									"sizes":[
										{
											"key":0,
											"value":0
										},
										{
											"key":1,
											"value":1
										}
									]
								},
								"gradientXMin":{
									"sizes":[]
								},
								"gradientXMax":{
									"sizes":[
										{
											"key":0,
											"value":0
										},
										{
											"key":1,
											"value":1
										}
									]
								},
								"gradientYMin":{
									"sizes":[]
								},
								"gradientYMax":{
									"sizes":[
										{
											"key":0,
											"value":0
										},
										{
											"key":1,
											"value":1
										}
									]
								},
								"gradientZMin":{
									"sizes":[]
								},
								"gradientZMax":{
									"sizes":[
										{
											"key":0,
											"value":0
										},
										{
											"key":1,
											"value":1
										}
									]
								}
							}
						},
						"rotationOverLifetime":{
							"enable":true,
							"angularVelocity":{
								"type":2,
								"separateAxes":false,
								"constant":45,
								"constantMin":-45,
								"constantMax":45,
								"constantMinSeparate":[
									0,
									0,
									-45
								],
								"constantMaxSeparate":[
									0,
									0,
									45
								],
								"gradient":{
									"angularVelocitys":[]
								},
								"gradientX":{
									"angularVelocitys":[]
								},
								"gradientY":{
									"angularVelocitys":[]
								},
								"gradientZ":{
									"angularVelocitys":[]
								},
								"gradientMin":{
									"angularVelocitys":[]
								},
								"gradientMax":{
									"angularVelocitys":[]
								},
								"gradientXMin":{
									"angularVelocitys":[]
								},
								"gradientXMax":{
									"angularVelocitys":[]
								},
								"gradientYMin":{
									"angularVelocitys":[]
								},
								"gradientYMax":{
									"angularVelocitys":[]
								},
								"gradientZMin":{
									"angularVelocitys":[]
								},
								"gradientZMax":{
									"angularVelocitys":[]
								}
							}
						},
						"renderMode":0,
						"stretchedBillboardCameraSpeedScale":0,
						"stretchedBillboardSpeedScale":0,
						"stretchedBillboardLengthScale":2,
						"sortingFudge":0,
						"material":{
							"type":"Laya.ShurikenParticleMaterial",
							"path":"Assets/tietu/mangci.lmat"
						}
					},
					"components":{},
					"child":[
						{
							"type":"ShuriKenParticle3D",
							"props":{
								"isStatic":false,
								"name":"lizi1"
							},
							"customProps":{
								"layer":0,
								"translate":[
									-0.14,
									-0.04,
									-0.67
								],
								"rotation":[
									-0.7054092,
									-0.003131057,
									-0.02258238,
									-0.7084336
								],
								"scale":[
									1,
									1,
									1
								],
								"isPerformanceMode":true,
								"duration":5,
								"looping":true,
								"prewarm":false,
								"startDelayType":0,
								"startDelay":0,
								"startDelayMin":0,
								"startDelayMax":0,
								"startLifetimeType":0,
								"startLifetimeConstant":2,
								"startLifetimeConstantMin":0,
								"startLifetimeConstantMax":2,
								"startLifetimeGradient":{
									"startLifetimes":[]
								},
								"startLifetimeGradientMin":{
									"startLifetimes":[]
								},
								"startLifetimeGradientMax":{
									"startLifetimes":[]
								},
								"startSpeedType":2,
								"startSpeedConstant":0.4,
								"startSpeedConstantMin":0.3,
								"startSpeedConstantMax":0.4,
								"threeDStartSize":false,
								"startSizeType":2,
								"startSizeConstant":0.2,
								"startSizeConstantMin":0.1,
								"startSizeConstantMax":0.2,
								"startSizeConstantSeparate":[
									0.2,
									1,
									1
								],
								"startSizeConstantMinSeparate":[
									0.1,
									1,
									1
								],
								"startSizeConstantMaxSeparate":[
									0.2,
									1,
									1
								],
								"threeDStartRotation":false,
								"startRotationType":0,
								"startRotationConstant":0,
								"startRotationConstantMin":0,
								"startRotationConstantMax":0,
								"startRotationConstantSeparate":[
									0,
									0,
									0
								],
								"startRotationConstantMinSeparate":[
									0,
									0,
									0
								],
								"startRotationConstantMaxSeparate":[
									0,
									0,
									0
								],
								"randomizeRotationDirection":0,
								"startColorType":0,
								"startColorConstant":[
									1,
									1,
									1,
									1
								],
								"startColorConstantMin":[
									0,
									0,
									0,
									0
								],
								"startColorConstantMax":[
									1,
									1,
									1,
									1
								],
								"gravity":[
									0,
									-9.81,
									0
								],
								"gravityModifier":0,
								"simulationSpace":1,
								"scaleMode":1,
								"playOnAwake":true,
								"maxParticles":1000,
								"autoRandomSeed":true,
								"randomSeed":85411362,
								"emission":{
									"enable":true,
									"emissionRate":500,
									"emissionRateTip":"Time",
									"bursts":[]
								},
								"shape":{
									"enable":true,
									"shapeType":0,
									"sphereRadius":313.9,
									"sphereEmitFromShell":false,
									"sphereRandomDirection":0,
									"hemiSphereRadius":313.9,
									"hemiSphereEmitFromShell":false,
									"hemiSphereRandomDirection":0,
									"coneAngle":25,
									"coneRadius":313.9,
									"coneLength":5,
									"coneEmitType":0,
									"coneRandomDirection":0,
									"boxX":1,
									"boxY":1,
									"boxZ":1,
									"boxRandomDirection":0,
									"circleRadius":313.9,
									"circleArc":360,
									"circleEmitFromEdge":false,
									"circleRandomDirection":0
								},
								"colorOverLifetime":{
									"enable":true,
									"color":{
										"type":1,
										"constant":[
											0,
											0,
											0,
											0
										],
										"gradient":{
											"alphas":[
												{
													"key":0,
													"value":0
												},
												{
													"key":0.4352941,
													"value":1
												},
												{
													"key":0.7617609,
													"value":1
												},
												{
													"key":1,
													"value":0
												}
											],
											"rgbs":[
												{
													"key":0,
													"value":[
														1,
														1,
														1
													]
												},
												{
													"key":1,
													"value":[
														1,
														1,
														1
													]
												}
											]
										},
										"constantMin":[
											0,
											0,
											0,
											0
										],
										"constantMax":[
											0,
											0,
											0,
											0
										],
										"gradientMax":{
											"alphas":[
												{
													"key":0,
													"value":0
												},
												{
													"key":0.4352941,
													"value":1
												},
												{
													"key":0.7617609,
													"value":1
												},
												{
													"key":1,
													"value":0
												}
											],
											"rgbs":[
												{
													"key":0,
													"value":[
														1,
														1,
														1
													]
												},
												{
													"key":1,
													"value":[
														1,
														1,
														1
													]
												}
											]
										}
									}
								},
								"renderMode":0,
								"stretchedBillboardCameraSpeedScale":0,
								"stretchedBillboardSpeedScale":0,
								"stretchedBillboardLengthScale":2,
								"sortingFudge":0,
								"material":{
									"type":"Laya.ShurikenParticleMaterial",
									"path":"Assets/tietu/gangdi.lmat"
								}
							},
							"components":{},
							"child":[
								{
									"type":"ShuriKenParticle3D",
									"props":{
										"isStatic":false,
										"name":"lizi2"
									},
									"customProps":{
										"layer":0,
										"translate":[
											0.17,
											3.59,
											0
										],
										"rotation":[
											0,
											-2.328306E-10,
											-6.984917E-10,
											-1
										],
										"scale":[
											1,
											1,
											1
										],
										"isPerformanceMode":true,
										"duration":5,
										"looping":true,
										"prewarm":false,
										"startDelayType":0,
										"startDelay":0,
										"startDelayMin":0,
										"startDelayMax":0,
										"startLifetimeType":0,
										"startLifetimeConstant":2,
										"startLifetimeConstantMin":0,
										"startLifetimeConstantMax":2,
										"startLifetimeGradient":{
											"startLifetimes":[]
										},
										"startLifetimeGradientMin":{
											"startLifetimes":[]
										},
										"startLifetimeGradientMax":{
											"startLifetimes":[]
										},
										"startSpeedType":2,
										"startSpeedConstant":0.4,
										"startSpeedConstantMin":0.3,
										"startSpeedConstantMax":0.4,
										"threeDStartSize":false,
										"startSizeType":2,
										"startSizeConstant":0.2,
										"startSizeConstantMin":0.1,
										"startSizeConstantMax":0.2,
										"startSizeConstantSeparate":[
											0.2,
											1,
											1
										],
										"startSizeConstantMinSeparate":[
											0.1,
											1,
											1
										],
										"startSizeConstantMaxSeparate":[
											0.2,
											1,
											1
										],
										"threeDStartRotation":false,
										"startRotationType":0,
										"startRotationConstant":0,
										"startRotationConstantMin":0,
										"startRotationConstantMax":0,
										"startRotationConstantSeparate":[
											0,
											0,
											0
										],
										"startRotationConstantMinSeparate":[
											0,
											0,
											0
										],
										"startRotationConstantMaxSeparate":[
											0,
											0,
											0
										],
										"randomizeRotationDirection":0,
										"startColorType":0,
										"startColorConstant":[
											1,
											1,
											1,
											1
										],
										"startColorConstantMin":[
											0,
											0,
											0,
											0
										],
										"startColorConstantMax":[
											1,
											1,
											1,
											1
										],
										"gravity":[
											0,
											-9.81,
											0
										],
										"gravityModifier":0,
										"simulationSpace":1,
										"scaleMode":1,
										"playOnAwake":true,
										"maxParticles":1000,
										"autoRandomSeed":true,
										"randomSeed":2536564277,
										"emission":{
											"enable":true,
											"emissionRate":50,
											"emissionRateTip":"Time",
											"bursts":[]
										},
										"shape":{
											"enable":true,
											"shapeType":0,
											"sphereRadius":108.5,
											"sphereEmitFromShell":false,
											"sphereRandomDirection":0,
											"hemiSphereRadius":108.5,
											"hemiSphereEmitFromShell":false,
											"hemiSphereRandomDirection":0,
											"coneAngle":25,
											"coneRadius":108.5,
											"coneLength":5,
											"coneEmitType":0,
											"coneRandomDirection":0,
											"boxX":1,
											"boxY":1,
											"boxZ":1,
											"boxRandomDirection":0,
											"circleRadius":108.5,
											"circleArc":360,
											"circleEmitFromEdge":false,
											"circleRandomDirection":0
										},
										"colorOverLifetime":{
											"enable":true,
											"color":{
												"type":1,
												"constant":[
													0,
													0,
													0,
													0
												],
												"gradient":{
													"alphas":[
														{
															"key":0,
															"value":0
														},
														{
															"key":0.4352941,
															"value":1
														},
														{
															"key":0.7617609,
															"value":1
														},
														{
															"key":1,
															"value":0
														}
													],
													"rgbs":[
														{
															"key":0,
															"value":[
																1,
																1,
																1
															]
														},
														{
															"key":1,
															"value":[
																1,
																1,
																1
															]
														}
													]
												},
												"constantMin":[
													0,
													0,
													0,
													0
												],
												"constantMax":[
													0,
													0,
													0,
													0
												],
												"gradientMax":{
													"alphas":[
														{
															"key":0,
															"value":0
														},
														{
															"key":0.4352941,
															"value":1
														},
														{
															"key":0.7617609,
															"value":1
														},
														{
															"key":1,
															"value":0
														}
													],
													"rgbs":[
														{
															"key":0,
															"value":[
																1,
																1,
																1
															]
														},
														{
															"key":1,
															"value":[
																1,
																1,
																1
															]
														}
													]
												}
											}
										},
										"renderMode":0,
										"stretchedBillboardCameraSpeedScale":0,
										"stretchedBillboardSpeedScale":0,
										"stretchedBillboardLengthScale":2,
										"sortingFudge":0,
										"material":{
											"type":"Laya.ShurikenParticleMaterial",
											"path":"Assets/tietu/gangdi.lmat"
										}
									},
									"components":{},
									"child":[]
								}
							]
						}
					]
				}
			]
		}
	]
}