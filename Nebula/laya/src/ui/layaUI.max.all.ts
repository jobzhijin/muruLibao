
import View=laya.ui.View;
import Dialog=laya.ui.Dialog;
module ui {
    export class BGUI extends View {
		public bg0:Laya.Image;
		public bg1:Laya.Image;

        public static  uiView:any ={"type":"View","props":{"width":640,"height":1136},"child":[{"type":"Image","props":{"var":"bg0","skin":"ui/bg.jpg","centerY":0,"centerX":0}},{"type":"Image","props":{"y":-107,"x":-55,"var":"bg1","skin":"ui/bg.jpg","centerY":0,"centerX":0}}]};
        constructor(){ super()}
        createChildren():void {
        
            super.createChildren();
            this.createView(ui.BGUI.uiView);

        }

    }
}

module ui {
    export class FGUI extends View {
		public ani1:Laya.FrameAnimation;
		public libao:Laya.Image;
		public tfxBg:Laya.Image;
		public tfx:Laya.Image;
		public fade:Laya.Sprite;

        public static  uiView:any ={"type":"View","props":{"width":640,"height":1136},"child":[{"type":"Image","props":{"var":"libao","skin":"ui/Home-page_1_27.png","scaleY":0.8,"scaleX":0.8,"right":20,"bottom":200}},{"type":"Image","props":{"var":"tfxBg","skin":"ui/tfx_bg.png","scaleY":0.8,"scaleX":0.8,"centerX":0,"bottom":65}},{"type":"Image","props":{"var":"tfx","skin":"ui/tfx_fg.png","scaleY":0.8,"scaleX":0.8,"centerX":0,"bottom":81}},{"type":"Sprite","props":{"var":"fade"}},{"type":"Image","props":{"y":341,"x":243,"skin":"ui/Tex.png"}},{"type":"Image","props":{"y":341,"x":240,"skin":"ui/JIan.png"},"compId":9},{"type":"Image","props":{"y":341,"x":390,"skin":"ui/JIan.png","scaleX":-1},"compId":10}],"animations":[{"nodes":[{"target":9,"keyframes":{"x":[{"value":233,"tweenMethod":"linearNone","tween":true,"target":9,"key":"x","index":0},{"value":241,"tweenMethod":"linearNone","tween":true,"target":9,"key":"x","index":15},{"value":233,"tweenMethod":"linearNone","tween":true,"target":9,"label":null,"key":"x","index":30}]}},{"target":10,"keyframes":{"x":[{"value":398,"tweenMethod":"linearNone","tween":true,"target":10,"key":"x","index":0},{"value":389,"tweenMethod":"linearNone","tween":true,"target":10,"key":"x","index":15},{"value":398,"tweenMethod":"linearNone","tween":true,"target":10,"label":null,"key":"x","index":30}]}}],"name":"ani1","id":1,"frameRate":24,"action":2}]};
        constructor(){ super()}
        createChildren():void {
        
            super.createChildren();
            this.createView(ui.FGUI.uiView);

        }

    }
}
