/**
* name 
*/

class NebulaScene extends  Laya.Scene{
	private nodes:Sprite3D[] = [];
	//private mainCamera:Laya.Camera;
	private camera:Sprite3D;
	private light:Laya.DirectionLight;
	private star:Sprite3D;
	private cameraScript:script.CameraScript;
	private assets:Sprite3D;
	private intro:IntroScene;
	private nebulaPS:Laya.ShuriKenParticle3D;
	private fg:view.FG;
	private rotateSelfScript:script.RotateSelfScript;
	private welcome:view.FadeText;
	private persons:Sprite3D[] = [];
	private sliderNodes:Sprite3D[]=[];
	private curSlideIndex:number = -1;
	private touchDownTime:number;
	private from:number;
	private static thisObj:NebulaScene;
	constructor(){
		super();
		this.init();		
	}

	private init():void{
		NebulaScene.thisObj = this;
		this.initUnityAssets();
		this.initNebulaPS();
		this.initStar();
		this.initCamera();
		this.addFg();
		
		this.light.removeSelf();
		this.rotateSelfScript = this.star.addComponent(script.RotateSelfScript) as script.RotateSelfScript;
		this.nebulaPS.particleSystem.play();
		setTimeout(()=> {
			this.fg.playUIEffect();
			this.cameraScript.playZoomInAnimation();
			setTimeout(()=> {
				Laya.Browser.window.addEventListener("startRoute",()=>{
					NebulaScene.thisObj.enableTouch();
				},false)
				this.enableTouch();
				this.welcome = new view.FadeText();
				this.welcome.show("ui/hy.png",GlobalVar.uiLayer,Laya.stage.width/2,Laya.stage.height/2,1,5000);
				this.initPersons();
				Laya.timer.frameLoop(1,this,()=>{
					this.sortPersons();
				}
			}, 2200);
		}, 100);
		
	}

	private addFg():void{
		this.fg = new view.FG();
		GlobalVar.uiLayer.addChild(this.fg);
	}

	private initUnityAssets():void{
		
		this.assets = Laya.loader.getRes("res/LayaScene_Nebula/Nebula.lh"); 
		this.light = this.assets.getChildByName("Directional Light") as Laya.DirectionLight;     

		this.addChild(this.assets);
	}

	private initStar():void{
		this.star = this.assets.getChildByName("Star") as Sprite3D;
		this.setModelPureWhite(this.star);
	}

	private setModelPureWhite(node:Laya.Node):void{
		for(let i = 0; i< node.numChildren;i++){
			let child = node.getChildAt(i);
			if(child instanceof Laya.MeshSprite3D){
				(child.meshRender.material as Laya.StandardMaterial).albedoColor = new Laya.Vector4(1,1,1,1);
			}
			else{
				this.setModelPureWhite(child);
			}
		}
	}

	private initNebulaPS():void{
		this.nebulaPS = this.assets.getChildByName("NebulaPS") as Laya.ShuriKenParticle3D;
	}

	private initCamera():void{
		this.camera = this.assets.getChildByName("Camera") as Sprite3D;
		this.cameraScript = this.camera.addComponent(script.CameraScript) as script.CameraScript;	
		GlobalVar.mainCamera = this.camera.getChildByName("rx").getChildByName("tz").getChildByName("Main Camera") as Laya.Camera;
		this.cameraScript.on(Laya.Event.DRAG_END,this,this.doSlide);
		this.cameraScript.on("OrienStopped",this,this.OrienStopped);
		this.cameraScript.on("OrienStarted",this,this.OrienStarted);
		let skyBox:Laya.SkyBox = new Laya.SkyBox();
		GlobalVar.mainCamera.clearFlag = Laya.BaseCamera.CLEARFLAG_SKY;
		GlobalVar.mainCamera.sky = skyBox;
		skyBox.textureCube = Laya.TextureCube.load("SkyCube.ltc");

	}

	private initNodes():void{
		for(let i=0;i<5;i++){
			let node = new TouchNode(new Laya.Vector3(2 + 2*i,0,0));
			node.on(Laya.Event.CHANGE,this,this.onNodeClick);
			GlobalVar.touchNodeLayer.addChild(node);
		}
	}

	private initPersons():void{
		let ary = [0,1,2,3,4,5,6,7,8,9];
		
		let personCount = Laya.Browser.window.personCount;
        for(let i=0;i<personCount;i++){

		//for(let i=0;i<10;i++){
			this.persons[i] = this.assets.getChildByName("Star").getChildByName("p"+ary[i]) as Sprite3D;
			let personScript = this.persons[i].addComponent(script.PersonScript) as script.PersonScript;
			personScript.index = ary[i];

			this.sliderNodes[i] = this.assets.getChildByName("Star").getChildByName("p"+ary[i]) as Sprite3D;
		}
	}

	private onNodeClick(pos:Laya.Vector3):void{
		// this.cameraScript.moveTo(pos);
	}

	private sortPersons():void{
		this.persons.sort((a,b)=>{
			let ascript = a.getComponentByType(script.PersonScript) as script.PersonScript;
			let bscript = b.getComponentByType(script.PersonScript) as script.PersonScript;
			if(ascript.zOrder >= bscript.zOrder){
				return 1;
			}else{
				return 0;
			}
		})
		
		for(let i=0;i<this.persons.length;i++){
			let title = (this.persons[i].getComponentByType(script.PersonScript) as script.PersonScript).title;
			title.removeSelf();
			GlobalVar.uiLayer.addChild(title);
		}
	}

	//启用星云场景的触摸功能
	public enableTouch(ev:any = null):void{
		Laya.stage.on(Laya.Event.MOUSE_DOWN,this,this.onTouchDown);
		Laya.stage.on(Laya.Event.MOUSE_UP,this,this.onTouchUp);
		this.cameraScript.enableTouch();
	}

	//禁用星云场景的触摸功能
	public disableTouch():void{
		Laya.stage.off(Laya.Event.MOUSE_DOWN,this,this.onTouchDown);
		Laya.stage.off(Laya.Event.MOUSE_UP,this,this.onTouchUp);
		this.cameraScript.disableTouch();
	} 

	private onTouchDown():void{
		this.touchDownTime = Date.now();
	}

	private onTouchUp(ev:Laya.Event):void{
		if(Date.now() - this.touchDownTime > 200) return;
		let rayCastHits:Laya.RaycastHit[] = [];
		let ray = new Laya.Ray(new Laya.Vector3(),new Laya.Vector3());
		GlobalVar.mainCamera.viewportPointToRay(new Laya.Vector2(ev.stageX,ev.stageY),ray);
		Laya.Physics.rayCastAll(ray,rayCastHits,50);
		for(let hit of rayCastHits){
			if(hit.sprite3D.name){
				let idx = hit.sprite3D.name.split("p")[1];
				Laya.Browser.window.selectStar(idx);
				this.disableTouch();
			}
			return;
		}
	}

	private doSlide(dir:number):void{
		this.curSlideIndex -= dir;
		if(this.curSlideIndex > this.sliderNodes.length - 1){
			this.curSlideIndex = 0;
		}
		else if(this.curSlideIndex < 0){
			this.curSlideIndex = this.sliderNodes.length - 1;
		}
		console.log(this.curSlideIndex);
		//this.cameraScript.moveTo(this.sliderNodes[this.curSlideIndex].transform.position,this.from,dir);
		this.from = this.curSlideIndex;
	}

	private OrienStopped():void{
		this.rotateSelfScript.state = script.RotateState.Free;
	}

	private OrienStarted():void{
		this.rotateSelfScript.state = script.RotateState.Lock;
		;
	}
}
