/**
* name 
*/
module script{
	export class NodeScript extends Laya.Script{
		private gameObject:Laya.Sprite3D;
		constructor(){
			super();
		}

		public _load(owner:Laya.Sprite3D):void{
			this.gameObject = owner as Laya.Sprite3D;
		}

		//组件加载完成后初始化 相当于Unity中的Start
		public _start(state:Laya.RenderState):void{
			//console.log(this.gameObject.transform.forward.x,this.gameObject.transform.forward.y,this.gameObject.transform.forward.z)
		}

		//每帧触发 相当于Unity中的Update
		public _update(state:Laya.RenderState):void{
			//let faceToScreenQ = new Laya.Quaternion();
			
			//Laya.Quaternion.rotationLookAt(new Laya.Vector3(-GlobalVar.mainCamera.transform.forward.x,GlobalVar.mainCamera.transform.forward.y,GlobalVar.mainCamera.transform.forward.z),Laya.Vector3.Up,faceToScreenQ);
			//console.log(faceToScreenQ.x,faceToScreenQ.y,faceToScreenQ.z);
			//this.gameObject.transform.rotation = faceToScreenQ;
		}

		
	}
}