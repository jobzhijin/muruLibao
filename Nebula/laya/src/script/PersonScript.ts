/**
* name 
*/
module script{
	export class PersonScript extends Laya.Script{
		private gameObject:Laya.Sprite3D;
		public index:number;
		public title:Laya.Image;
		public zOrder:number = 0;
		constructor(){
			super();
		}

		public _load(owner:Laya.Sprite3D):void{
			this.gameObject = owner as Laya.Sprite3D;
		}

		//组件加载完成后初始化 相当于Unity中的Start
		public _start(state:Laya.RenderState):void{
			this.title = new Laya.Image();
			
			let brandName = Laya.Browser.window.brandName;
            this.title.loadImage("ui/"+brandName+"/p"+this.index+".png",0,0,0,0,Laya.Handler.create(this,()=>{ 

			//this.title.loadImage("ui/p"+this.index+".png",0,0,0,0,Laya.Handler.create(this,()=>{
				//this.title.scale(0.7,0.7);
				this.title.pivotY = this.title.height;
			},null,true));
			GlobalVar.uiLayer.addChild(this.title);
			//console.log(this.gameObject.transform.forward.x,this.gameObject.transform.forward.y,this.gameObject.transform.forward.z)
		}

		//每帧触发 相当于Unity中的Update
		public _update(state:Laya.RenderState):void{
			let screenPos = new Laya.Vector3();
			GlobalVar.mainCamera.worldToViewportPoint(this.gameObject.transform.position,screenPos);
			this.title.x = screenPos.x;
			this.title.y = screenPos.y;
			this.zOrder = screenPos.z;
			if(screenPos.z > 0.98){
				this.title.alpha = screenPos.z * 0.5;				
			}
			else{
				this.title.alpha = 1;
			}
			let scaleValue = (1 - screenPos.z) / 0.04 + 0.2;
			if(scaleValue > 0.7) scaleValue = 0.7;
			this.title.scale(scaleValue ,scaleValue);
		}
	}
}