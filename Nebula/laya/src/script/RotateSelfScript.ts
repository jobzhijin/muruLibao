/**
* name 
*/
module script{
	export class RotateSelfScript extends Laya.Script{
		private gameObject:Laya.Sprite3D;
		public state:RotateState = RotateState.Free;
		constructor(){
			super();
		}

		public _load(owner:Laya.Sprite3D):void{
			this.gameObject = owner as Laya.Sprite3D;
		}

		//组件加载完成后初始化 相当于Unity中的Start
		public _start(state:Laya.RenderState):void{

		}

		//每帧触发 相当于Unity中的Update
		public _update(state:Laya.RenderState):void{
			if(this.state == RotateState.Free)
				this.gameObject.transform.rotate(new Laya.Vector3(0,-0.0002,0),true);
		}
	}

	export enum RotateState {Free, Lock}
}