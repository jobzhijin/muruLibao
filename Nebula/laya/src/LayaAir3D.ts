// 程序入口
class LayaAir3D {
    private intro:IntroScene;
    private boom:BoomScene;
    private allLoaded:boolean = false;
    private lockIntro:boolean = true;
    private fadeText:view.FadeText;
    public static thisObj:LayaAir3D;
    private canvasPreloaded:boolean = false;
    private domPreloaded:boolean = false;
    constructor() {
        //初始化引擎
        Laya3D.init(640, 1136, true);
        Laya.stage.bgColor = "#ffffff";
        //适配模式
        Laya.stage.scaleMode = Laya.Stage.SCALE_FIXED_AUTO;
        Laya.stage.screenMode = Laya.Stage.SCREEN_VERTICAL;
        Laya.ResourceVersion.type = Laya.ResourceVersion.FILENAME_VERSION;

        let bgLayer = new Laya.Sprite();
        GlobalVar.bgLayer = bgLayer;
        let touchNodeLayer = new Laya.Sprite();
        GlobalVar.touchNodeLayer = touchNodeLayer;
        let uiLayer = new Laya.Sprite();
        GlobalVar.uiLayer = uiLayer;
        Laya.stage.addChild(bgLayer);
        Laya.stage.addChild(touchNodeLayer);
        Laya.stage.addChild(uiLayer);
        
            //加载版本信息文件
        Laya.ResourceVersion.enable("version.json", Laya.Handler.create(this, this.preloadIntro));  

        Laya.Browser.window.addEventListener("dompreloaded",()=>{
            LayaAir3D.thisObj.domPreloadedHandler();
        },false)
        
        //this.loadBaseAssets();
    }

    private preloadIntro():void{
        Laya.loader.load([
            {url:"res/atlas/ui.atlas"},
            {url:"ui/bg.jpg"}
            /*,
            {url:"nx.jpg", type:Laya.TextureCube},
            {url:"ny.jpg", type:Laya.TextureCube},
            {url:"nz.jpg", type:Laya.TextureCube},
            {url:"px.jpg", type:Laya.TextureCube},
            {url:"py.jpg", type:Laya.TextureCube},
            {url:"pz.jpg", type:Laya.TextureCube}
            */
        ],Laya.Handler.create(this,this.preloadUnityAssets));
    }


    private preloadUnityAssets():void{
        var urls = [
                "res/LayaScene_Intro/Intro.lh",
                "SkyCube.ltc"
            ]
        Laya.loader.create(urls,Laya.Handler.create(this,this.addBG));
    }

    private addBG():void{   
        //let bg = new view.BG();
		//GlobalVar.bgLayer.addChild(bg);
        //bg.scroll();
        //bg.playUIEffect();
        this.showTopLogo();
        this.playIntro();
        this.centerText();
    }

    private showTopLogo():void{
        // let img = new Laya.Image("ui/Home-page_1_03.png");
        // GlobalVar.uiLayer.addChild(img);
    }

    private centerText():void{
        this.fadeText = new view.FadeText();
        this.fadeText.show("ui/ma.png",GlobalVar.uiLayer,Laya.stage.width/2+5,Laya.stage.height/2,0.8,800);
        setTimeout(function() {
            this.fadeText = new view.FadeText();
            this.fadeText.show("ui/mr.png",GlobalVar.uiLayer,Laya.stage.width/2+5,Laya.stage.height/2,0.8,800);
            setTimeout(function() {
                this.fadeText = new view.FadeText();
                this.fadeText.show("ui/zl.png",GlobalVar.uiLayer,Laya.stage.width/2+5,Laya.stage.height/2,0.8,800);
                setTimeout(function() {
                    this.fadeText = new view.FadeText();
                    this.fadeText.show("ui/tf.png",GlobalVar.uiLayer,Laya.stage.width/2+5,Laya.stage.height/2,0.8,800);
                }, 3000);
            }, 3000);
        }, 3000);
    }

    private playIntro():void{
        this.intro = new IntroScene();
        Laya.stage.addChildAt(this.intro,1);
        setTimeout(() =>{
            if(this.allLoaded){
                this.playBoom();
            }
            else{
                this.lockIntro = false;
            }
        },9800);
        this.loadBaseAssets();
    }

    private loadBaseAssets():void{
        Laya.loader.load([
            {url:"ani/Home1.png"},
            {url:"ani/Home2.png"},
            {url:"ani/Home3.png"},
            {url:"ani/Home4.png"},
            {url:"ani/Home5.png"},
            {url:"ani/Home6.png"}
        ],Laya.Handler.create(this,this.loadUnityAssets));
    }

    private loadUnityAssets():void{
        var urls = [
                "res/LayaScene_Nebula/Nebula.lh",
                "res/LayaScene_Boom/Boom.lh"
            ]
        Laya.loader.create(urls,Laya.Handler.create(this,this.canvasPreloadedHandler));
    }

    private canvasPreloadedHandler():void{
        Laya.timer.loop(1000,this,this.loopH5Load);
        // console.log(Laya.Browser.window);
        // this.canvasPreloaded = true;
        // if(this.domPreloaded||Laya.Browser.window.h5LoadStatus){
            // this.onAllLoad();
        // }
    }
    private loopH5Load():void
    {
        if(Laya.Browser.window.h5LoadStatus)
        {
            Laya.timer.clear(this,this.loopH5Load);
            this.onAllLoad();
        }
    }

    private domPreloadedHandler():void{
        console.log("页面加载完成");
        this.domPreloaded = true;
        if(this.canvasPreloaded){
            this.onAllLoad();
        }
    }

    private onAllLoad():void{
        this.allLoaded = true;
        if(!this.lockIntro){
            this.playBoom();
        }
    }

    private playBoom():void{
        this.intro.dispose();
        this.boom = new BoomScene();
        Laya.stage.addChildAt(this.boom,1);
        setTimeout(()=> {
            this.playNebula();
        }, 800);
    }

    private playNebula():void{
        this.boom.clearSkybox();
        setTimeout(()=> {
            this.boom.dispose();
        }, 5000);
       //
        var nebulaScene = new NebulaScene();
        GlobalVar.tdLayer = nebulaScene;
        Laya.stage.addChildAt(nebulaScene,1);
    }
}
new LayaAir3D();