/*
* name;
*/
class TouchNode extends Laya.Sprite{
    private pos3:Laya.Vector3;
    private img:Laya.Sprite;
    constructor(pos3:Laya.Vector3){
        super();
        this.pos3 = pos3;
        this.init();
    }

    private init():void{
        this.img = new Laya.Sprite();
        this.img.loadImage("res/node.png",0,0,0,0);
        this.img.x = -32;
        this.img.y = -32;
        this.addChild(this.img);
        this.img.on(Laya.Event.CLICK,this,this.onClick);
        Laya.timer.frameLoop(1, this, this.onFrame);
    }

    private onClick():void{
        this.event(Laya.Event.CHANGE,this.pos3.clone());
    }

    private onFrame():void{
        let pos2 = new Laya.Vector3();
        GlobalVar.mainCamera.worldToViewportPoint(this.pos3, pos2);
        this.x = pos2.x;
        this.y = pos2.y;
        let scaleFactor = 20 / Laya.Vector3.distance(GlobalVar.mainCamera.transform.position, this.pos3);
        this.scale(scaleFactor, scaleFactor);
        if(pos2.z >= 1){
            this.visible = false;
        }
        else{
            this.visible = true;
        }
    }
}