/**Created by the LayaAirIDE*/
module view{
	export class BG extends ui.BGUI{
		constructor(){
			super();
			this.width = Laya.stage.width;
			this.height = Laya.stage.height;
		}

		createChildren():void{
			super.createChildren();
		}

		public playUIEffect():void{
			setInterval(()=>{
				this.createStarParticle();
			}, 150);
		}

		public scroll():void{
			Laya.timer.frameLoop(1,this,this.scrollHandler);
		}

		private scrollHandler(){
			if(this.bg0.x < 0){
				this.bg1.x = this.bg0.x + this.bg0.width;
			}
			if(this.bg1.x < 0){
				this.bg0.x = this.bg1.x + this.bg1.width;
			}
			this.bg0.x -= 0.2;
			this.bg1.x -=0.2;
		}

		private createStarParticle():void{
			let idx = Math.ceil(Math.random()*3);
			let s = new Laya.Sprite();
			s.loadImage("ui/s"+idx+".png");
			s.alpha = 0;
			s.x = Math.random() * Laya.stage.width;
			s.y = Math.random() * Laya.stage.height;
			this.addChild(s);
			Laya.Tween.to(s,{alpha:1},200,Laya.Ease.linearNone,new laya.utils.Handler(this,()=>{
				Laya.Tween.to(s,{alpha:0},100,Laya.Ease.linearNone,new laya.utils.Handler(this,()=>{
					s.removeSelf();
					s.destroy(true);
				}));
			}));
		}

		public fadeIn():void{
			Laya.Tween.to(this,{alpha:1},1000,Laya.Ease.linearNone);
		}
	}
}