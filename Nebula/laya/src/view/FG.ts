/**Created by the LayaAirIDE*/
module view{
	export class FG extends ui.FGUI{
		constructor(){
			super();
			this.width = Laya.stage.width;
			this.height = Laya.stage.height;
		}

		createChildren():void{
			super.createChildren();
			this.libao.on(Laya.Event.CLICK,this,this.openLibao);
			this.tfx.on(Laya.Event.CLICK,this,this.openTFX);
		}

		private doFade():void{
			Laya.Tween.to(this.fade,{alpha:0},2000,Laya.Ease.linearNone,Laya.Handler.create(this,()=>{
				this.fade.removeSelf();
			}));
	
		}

		public playUIEffect():void{
			this.logoAlpha1();
			this.tfxAlpha0();
			this.libaoFlowUp();
		}

		private logoAlpha1():void{
			//Laya.Tween.to(this.logo,{alpha:1},4000,Laya.Ease.linearNone);
		}

		private tfxAlpha0(){
			Laya.Tween.to(this.tfxBg,{alpha:0},2000,Laya.Ease.linearNone,new laya.utils.Handler(this,()=>{
				this.tfxAlpha1();
			}));
		}

		private tfxAlpha1(){
			Laya.Tween.to(this.tfxBg,{alpha:1},2000,Laya.Ease.linearNone,new laya.utils.Handler(this,()=>{
				this.tfxAlpha0();
			}));
		}

		private libaoFlowUp():void{
			Laya.Tween.to(this.libao,{y:this.libao.y - 30},3000,Laya.Ease.linearNone,new laya.utils.Handler(this,()=>{
				this.libaoFlowDown();
			}));
		}

		private libaoFlowDown():void{
			Laya.Tween.to(this.libao,{y:this.libao.y + 30},3000,Laya.Ease.linearNone,new laya.utils.Handler(this,()=>{
				this.libaoFlowUp();
			}));
		}

		private openLibao():void{
			Laya.Browser.window.lingqu();
		}

		private openTFX():void{
			Laya.Browser.window.dingzhi();
		}
	}
}