/**
* name 
*/
module view{
	export class FadeText extends Laya.Sprite{
		private txt:Laya.Sprite;
		private _scale:number;
		constructor(){
			super();
			this.txt = new Laya.Sprite();
			this.addChild(this.txt);
			this.alpha = 0;
		}

		public show(name:string, parent:Laya.Node,x:number,y:number,scale:number = 1,hideTimeout?:number):void{
			this._scale = scale;
			this.txt.loadImage(name,0,0,0,0,Laya.Handler.create(this,this.center,null,true));
			this.alpha1();
			parent.addChild(this);
			this.x = x;
			this.y = y;
			if(hideTimeout != 0 && hideTimeout != null){
				setTimeout(() =>{
					this.alpha0();
				}, hideTimeout);
			}
		}

		public hide():void{
			this.alpha0();
		}

		private center():void{
			this.txt.scale(this._scale,this._scale);
			this.txt.x = -this.txt.width * this._scale / 2;
			this.txt.y = -this.txt.height * this._scale / 2;	
		}

		private alpha0(){
			Laya.Tween.to(this,{alpha:0},1700,Laya.Ease.linearNone,Laya.Handler.create(this,()=>{
				this.removeSelf();
				this.destroy(true);
			},null,true));
		}

		private alpha1(){
			Laya.Tween.to(this,{alpha:1},600,Laya.Ease.linearNone,Laya.Handler.create(this,()=>{

			},null,true));
		}
	}
}