/*
* name;
*/
class BoomScene extends  Laya.Scene{
    private assets:Sprite3D;
     constructor(){
        super();
        this.init();
    }

    private init():void{
        this.initUnityAssets();
    }

    private initUnityAssets():void{
        this.assets = Laya.loader.getRes("res/LayaScene_Boom/Boom.lh"); 
        this.addChild(this.assets);  
        let skyBox:Laya.SkyBox = new Laya.SkyBox();
		let camera = this.assets.getChildByName("Main Camera") as Laya.Camera;
        camera.clearFlag = Laya.BaseCamera.CLEARFLAG_SKY;
		camera.sky = skyBox;
		skyBox.textureCube = Laya.TextureCube.load("SkyCube.ltc");
        
    }

    public clearSkybox():void{
        (this.assets.getChildByName("Main Camera") as Laya.Camera).clearFlag = Laya.BaseCamera.CLEARFLAG_NONE;
    }

    public dispose():void{
        setTimeout(()=> {
            this.removeSelf();
            this.destroy(true);
        }, 300);
       
    }
}