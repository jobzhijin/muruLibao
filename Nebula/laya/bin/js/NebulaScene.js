/**
* name
*/
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var NebulaScene = /** @class */ (function (_super) {
    __extends(NebulaScene, _super);
    function NebulaScene() {
        var _this = _super.call(this) || this;
        _this.nodes = [];
        _this.persons = [];
        _this.sliderNodes = [];
        _this.curSlideIndex = -1;
        _this.init();
        return _this;
    }
    NebulaScene.prototype.init = function () {
        var _this = this;
        NebulaScene.thisObj = this;
        this.initUnityAssets();
        this.initNebulaPS();
        this.initStar();
        this.initCamera();
        this.addFg();
        this.light.removeSelf();
        this.rotateSelfScript = this.star.addComponent(script.RotateSelfScript);
        this.nebulaPS.particleSystem.play();
        setTimeout(function () {
            _this.fg.playUIEffect();
            _this.cameraScript.playZoomInAnimation();
            setTimeout(function () {
                Laya.Browser.window.addEventListener("startRoute", function () {
                    NebulaScene.thisObj.enableTouch();
                }, false);
                _this.enableTouch();
                _this.welcome = new view.FadeText();
                _this.welcome.show("ui/hy.png", GlobalVar.uiLayer, Laya.stage.width / 2, Laya.stage.height / 2, 1, 5000);
                _this.initPersons();
                Laya.timer.frameLoop(1, _this, function () {
                    _this.sortPersons();
                });
            }, 2200);
        }, 100);
    };
    NebulaScene.prototype.addFg = function () {
        this.fg = new view.FG();
        GlobalVar.uiLayer.addChild(this.fg);
    };
    NebulaScene.prototype.initUnityAssets = function () {
        this.assets = Laya.loader.getRes("res/LayaScene_Nebula/Nebula.lh");
        this.light = this.assets.getChildByName("Directional Light");
        this.addChild(this.assets);
    };
    NebulaScene.prototype.initStar = function () {
        this.star = this.assets.getChildByName("Star");
        this.setModelPureWhite(this.star);
    };
    NebulaScene.prototype.setModelPureWhite = function (node) {
        for (var i = 0; i < node.numChildren; i++) {
            var child = node.getChildAt(i);
            if (child instanceof Laya.MeshSprite3D) {
                child.meshRender.material.albedoColor = new Laya.Vector4(1, 1, 1, 1);
            }
            else {
                this.setModelPureWhite(child);
            }
        }
    };
    NebulaScene.prototype.initNebulaPS = function () {
        this.nebulaPS = this.assets.getChildByName("NebulaPS");
    };
    NebulaScene.prototype.initCamera = function () {
        this.camera = this.assets.getChildByName("Camera");
        this.cameraScript = this.camera.addComponent(script.CameraScript);
        GlobalVar.mainCamera = this.camera.getChildByName("rx").getChildByName("tz").getChildByName("Main Camera");
        this.cameraScript.on(Laya.Event.DRAG_END, this, this.doSlide);
        this.cameraScript.on("OrienStopped", this, this.OrienStopped);
        this.cameraScript.on("OrienStarted", this, this.OrienStarted);
        var skyBox = new Laya.SkyBox();
        GlobalVar.mainCamera.clearFlag = Laya.BaseCamera.CLEARFLAG_SKY;
        GlobalVar.mainCamera.sky = skyBox;
        skyBox.textureCube = Laya.TextureCube.load("SkyCube.ltc");
    };
    NebulaScene.prototype.initNodes = function () {
        for (var i = 0; i < 5; i++) {
            var node = new TouchNode(new Laya.Vector3(2 + 2 * i, 0, 0));
            node.on(Laya.Event.CHANGE, this, this.onNodeClick);
            GlobalVar.touchNodeLayer.addChild(node);
        }
    };
    NebulaScene.prototype.initPersons = function () {
        var ary = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        var personCount = Laya.Browser.window.personCount;
        for (var i = 0; i < personCount; i++) {
            //for(let i=0;i<10;i++){
            this.persons[i] = this.assets.getChildByName("Star").getChildByName("p" + ary[i]);
            var personScript = this.persons[i].addComponent(script.PersonScript);
            personScript.index = ary[i];
            this.sliderNodes[i] = this.assets.getChildByName("Star").getChildByName("p" + ary[i]);
        }
    };
    NebulaScene.prototype.onNodeClick = function (pos) {
        // this.cameraScript.moveTo(pos);
    };
    NebulaScene.prototype.sortPersons = function () {
        this.persons.sort(function (a, b) {
            var ascript = a.getComponentByType(script.PersonScript);
            var bscript = b.getComponentByType(script.PersonScript);
            if (ascript.zOrder >= bscript.zOrder) {
                return 1;
            }
            else {
                return 0;
            }
        });
        for (var i = 0; i < this.persons.length; i++) {
            var title = this.persons[i].getComponentByType(script.PersonScript).title;
            title.removeSelf();
            GlobalVar.uiLayer.addChild(title);
        }
    };
    //启用星云场景的触摸功能
    NebulaScene.prototype.enableTouch = function (ev) {
        if (ev === void 0) { ev = null; }
        Laya.stage.on(Laya.Event.MOUSE_DOWN, this, this.onTouchDown);
        Laya.stage.on(Laya.Event.MOUSE_UP, this, this.onTouchUp);
        this.cameraScript.enableTouch();
    };
    //禁用星云场景的触摸功能
    NebulaScene.prototype.disableTouch = function () {
        Laya.stage.off(Laya.Event.MOUSE_DOWN, this, this.onTouchDown);
        Laya.stage.off(Laya.Event.MOUSE_UP, this, this.onTouchUp);
        this.cameraScript.disableTouch();
    };
    NebulaScene.prototype.onTouchDown = function () {
        this.touchDownTime = Date.now();
    };
    NebulaScene.prototype.onTouchUp = function (ev) {
        if (Date.now() - this.touchDownTime > 200)
            return;
        var rayCastHits = [];
        var ray = new Laya.Ray(new Laya.Vector3(), new Laya.Vector3());
        GlobalVar.mainCamera.viewportPointToRay(new Laya.Vector2(ev.stageX, ev.stageY), ray);
        Laya.Physics.rayCastAll(ray, rayCastHits, 50);
        for (var _i = 0, rayCastHits_1 = rayCastHits; _i < rayCastHits_1.length; _i++) {
            var hit = rayCastHits_1[_i];
            if (hit.sprite3D.name) {
                var idx = hit.sprite3D.name.split("p")[1];
                Laya.Browser.window.selectStar(idx);
                this.disableTouch();
            }
            return;
        }
    };
    NebulaScene.prototype.doSlide = function (dir) {
        this.curSlideIndex -= dir;
        if (this.curSlideIndex > this.sliderNodes.length - 1) {
            this.curSlideIndex = 0;
        }
        else if (this.curSlideIndex < 0) {
            this.curSlideIndex = this.sliderNodes.length - 1;
        }
        console.log(this.curSlideIndex);
        //this.cameraScript.moveTo(this.sliderNodes[this.curSlideIndex].transform.position,this.from,dir);
        this.from = this.curSlideIndex;
    };
    NebulaScene.prototype.OrienStopped = function () {
        this.rotateSelfScript.state = script.RotateState.Free;
    };
    NebulaScene.prototype.OrienStarted = function () {
        this.rotateSelfScript.state = script.RotateState.Lock;
        ;
    };
    return NebulaScene;
}(Laya.Scene));
//# sourceMappingURL=NebulaScene.js.map