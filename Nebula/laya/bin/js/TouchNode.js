var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/*
* name;
*/
var TouchNode = /** @class */ (function (_super) {
    __extends(TouchNode, _super);
    function TouchNode(pos3) {
        var _this = _super.call(this) || this;
        _this.pos3 = pos3;
        _this.init();
        return _this;
    }
    TouchNode.prototype.init = function () {
        this.img = new Laya.Sprite();
        this.img.loadImage("res/node.png", 0, 0, 0, 0);
        this.img.x = -32;
        this.img.y = -32;
        this.addChild(this.img);
        this.img.on(Laya.Event.CLICK, this, this.onClick);
        Laya.timer.frameLoop(1, this, this.onFrame);
    };
    TouchNode.prototype.onClick = function () {
        this.event(Laya.Event.CHANGE, this.pos3.clone());
    };
    TouchNode.prototype.onFrame = function () {
        var pos2 = new Laya.Vector3();
        GlobalVar.mainCamera.worldToViewportPoint(this.pos3, pos2);
        this.x = pos2.x;
        this.y = pos2.y;
        var scaleFactor = 20 / Laya.Vector3.distance(GlobalVar.mainCamera.transform.position, this.pos3);
        this.scale(scaleFactor, scaleFactor);
        if (pos2.z >= 1) {
            this.visible = false;
        }
        else {
            this.visible = true;
        }
    };
    return TouchNode;
}(Laya.Sprite));
//# sourceMappingURL=TouchNode.js.map