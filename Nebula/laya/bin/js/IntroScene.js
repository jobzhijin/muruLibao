var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/*
* name;
*/
var Sprite3D = Laya.Sprite3D;
var IntroScene = /** @class */ (function (_super) {
    __extends(IntroScene, _super);
    function IntroScene() {
        var _this = _super.call(this) || this;
        _this.init();
        return _this;
    }
    IntroScene.prototype.init = function () {
        this.initUnityAssets();
    };
    IntroScene.prototype.initUnityAssets = function () {
        this.assets = Laya.loader.getRes("res/LayaScene_Intro/Intro.lh");
        this.addChild(this.assets);
        var skyBox = new Laya.SkyBox();
        var camera = this.assets.getChildByName("Main Camera");
        camera.clearFlag = Laya.BaseCamera.CLEARFLAG_SKY;
        camera.sky = skyBox;
        skyBox.textureCube = Laya.TextureCube.load("SkyCube.ltc");
    };
    IntroScene.prototype.dispose = function () {
        var _this = this;
        setTimeout(function () {
            _this.removeSelf();
            _this.destroy(true);
        }, 300);
    };
    return IntroScene;
}(Laya.Scene));
//# sourceMappingURL=IntroScene.js.map