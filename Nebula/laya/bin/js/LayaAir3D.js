// 程序入口
var LayaAir3D = /** @class */ (function () {
    function LayaAir3D() {
        this.allLoaded = false;
        this.lockIntro = true;
        this.canvasPreloaded = false;
        this.domPreloaded = false;
        //初始化引擎
        Laya3D.init(640, 1136, true);
        Laya.stage.bgColor = "#ffffff";
        //适配模式
        Laya.stage.scaleMode = Laya.Stage.SCALE_FIXED_AUTO;
        Laya.stage.screenMode = Laya.Stage.SCREEN_VERTICAL;
        Laya.ResourceVersion.type = Laya.ResourceVersion.FILENAME_VERSION;
        var bgLayer = new Laya.Sprite();
        GlobalVar.bgLayer = bgLayer;
        var touchNodeLayer = new Laya.Sprite();
        GlobalVar.touchNodeLayer = touchNodeLayer;
        var uiLayer = new Laya.Sprite();
        GlobalVar.uiLayer = uiLayer;
        Laya.stage.addChild(bgLayer);
        Laya.stage.addChild(touchNodeLayer);
        Laya.stage.addChild(uiLayer);
        //加载版本信息文件
        Laya.ResourceVersion.enable("version.json", Laya.Handler.create(this, this.preloadIntro));
        Laya.Browser.window.addEventListener("dompreloaded", function () {
            LayaAir3D.thisObj.domPreloadedHandler();
        }, false);
        //this.loadBaseAssets();
    }
    LayaAir3D.prototype.preloadIntro = function () {
        Laya.loader.load([
            { url: "res/atlas/ui.atlas" },
            { url: "ui/bg.jpg" }
            /*,
            {url:"nx.jpg", type:Laya.TextureCube},
            {url:"ny.jpg", type:Laya.TextureCube},
            {url:"nz.jpg", type:Laya.TextureCube},
            {url:"px.jpg", type:Laya.TextureCube},
            {url:"py.jpg", type:Laya.TextureCube},
            {url:"pz.jpg", type:Laya.TextureCube}
            */
        ], Laya.Handler.create(this, this.preloadUnityAssets));
    };
    LayaAir3D.prototype.preloadUnityAssets = function () {
        var urls = [
            "res/LayaScene_Intro/Intro.lh",
            "SkyCube.ltc"
        ];
        Laya.loader.create(urls, Laya.Handler.create(this, this.addBG));
    };
    LayaAir3D.prototype.addBG = function () {
        //let bg = new view.BG();
        //GlobalVar.bgLayer.addChild(bg);
        //bg.scroll();
        //bg.playUIEffect();
        this.showTopLogo();
        this.playIntro();
        this.centerText();
    };
    LayaAir3D.prototype.showTopLogo = function () {
        // let img = new Laya.Image("ui/Home-page_1_03.png");
        // GlobalVar.uiLayer.addChild(img);
    };
    LayaAir3D.prototype.centerText = function () {
        this.fadeText = new view.FadeText();
        this.fadeText.show("ui/ma.png", GlobalVar.uiLayer, Laya.stage.width / 2 + 5, Laya.stage.height / 2, 0.8, 800);
        setTimeout(function () {
            this.fadeText = new view.FadeText();
            this.fadeText.show("ui/mr.png", GlobalVar.uiLayer, Laya.stage.width / 2 + 5, Laya.stage.height / 2, 0.8, 800);
            setTimeout(function () {
                this.fadeText = new view.FadeText();
                this.fadeText.show("ui/zl.png", GlobalVar.uiLayer, Laya.stage.width / 2 + 5, Laya.stage.height / 2, 0.8, 800);
                setTimeout(function () {
                    this.fadeText = new view.FadeText();
                    this.fadeText.show("ui/tf.png", GlobalVar.uiLayer, Laya.stage.width / 2 + 5, Laya.stage.height / 2, 0.8, 800);
                }, 3000);
            }, 3000);
        }, 3000);
    };
    LayaAir3D.prototype.playIntro = function () {
        var _this = this;
        this.intro = new IntroScene();
        Laya.stage.addChildAt(this.intro, 1);
        setTimeout(function () {
            if (_this.allLoaded) {
                _this.playBoom();
            }
            else {
                _this.lockIntro = false;
            }
        }, 9800);
        this.loadBaseAssets();
    };
    LayaAir3D.prototype.loadBaseAssets = function () {
        Laya.loader.load([
            { url: "ani/Home1.png" },
            { url: "ani/Home2.png" },
            { url: "ani/Home3.png" },
            { url: "ani/Home4.png" },
            { url: "ani/Home5.png" },
            { url: "ani/Home6.png" }
        ], Laya.Handler.create(this, this.loadUnityAssets));
    };
    LayaAir3D.prototype.loadUnityAssets = function () {
        var urls = [
            "res/LayaScene_Nebula/Nebula.lh",
            "res/LayaScene_Boom/Boom.lh"
        ];
        Laya.loader.create(urls, Laya.Handler.create(this, this.canvasPreloadedHandler));
    };
    LayaAir3D.prototype.canvasPreloadedHandler = function () {
        Laya.timer.loop(1000, this, this.loopH5Load);
        // console.log(Laya.Browser.window);
        // this.canvasPreloaded = true;
        // if(this.domPreloaded||Laya.Browser.window.h5LoadStatus){
        // this.onAllLoad();
        // }
    };
    LayaAir3D.prototype.loopH5Load = function () {
        if (Laya.Browser.window.h5LoadStatus) {
            Laya.timer.clear(this, this.loopH5Load);
            this.onAllLoad();
        }
    };
    LayaAir3D.prototype.domPreloadedHandler = function () {
        console.log("页面加载完成");
        this.domPreloaded = true;
        if (this.canvasPreloaded) {
            this.onAllLoad();
        }
    };
    LayaAir3D.prototype.onAllLoad = function () {
        this.allLoaded = true;
        if (!this.lockIntro) {
            this.playBoom();
        }
    };
    LayaAir3D.prototype.playBoom = function () {
        var _this = this;
        this.intro.dispose();
        this.boom = new BoomScene();
        Laya.stage.addChildAt(this.boom, 1);
        setTimeout(function () {
            _this.playNebula();
        }, 800);
    };
    LayaAir3D.prototype.playNebula = function () {
        var _this = this;
        this.boom.clearSkybox();
        setTimeout(function () {
            _this.boom.dispose();
        }, 5000);
        //
        var nebulaScene = new NebulaScene();
        GlobalVar.tdLayer = nebulaScene;
        Laya.stage.addChildAt(nebulaScene, 1);
    };
    return LayaAir3D;
}());
new LayaAir3D();
//# sourceMappingURL=LayaAir3D.js.map