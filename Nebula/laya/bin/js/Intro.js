var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/*
* name;
*/
var Sprite3D = Laya.Sprite3D;
var Intro = /** @class */ (function (_super) {
    __extends(Intro, _super);
    function Intro() {
        var _this = _super.call(this) || this;
        _this.init();
        return _this;
    }
    Intro.prototype.init = function () {
        this.initUnityAssets();
    };
    Intro.prototype.play2DAni = function () {
        var _this = this;
        this.ani2D = new Laya.Animation();
        this.ani2D.loadAnimation("Intro.ani", Laya.Handler.create(this, function () {
            _this.ani2D.play(0, true, "shine");
        }));
        this.ani2D.x = Laya.stage.width / 2;
        this.ani2D.y = -20 + Laya.stage.height / 2;
        GlobalVar.uiLayer.addChild(this.ani2D);
    };
    Intro.prototype.initUnityAssets = function () {
        this.assets = Laya.loader.getRes("res/LayaScene_Intro/Intro.lh");
        for (var i = 0; i < this.assets.numChildren; i++) {
            var gameObject = this.assets.getChildAt(i);
        }
        this.addChild(this.assets);
    };
    Intro.prototype.dispose = function () {
        var _this = this;
        this.removeSelf();
        setTimeout(function () {
            _this.destroy(true);
        }, 100);
    };
    return Intro;
}(Laya.Scene));
//# sourceMappingURL=Intro.js.map