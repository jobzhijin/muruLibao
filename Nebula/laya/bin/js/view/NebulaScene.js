var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name
*/
var view;
(function (view) {
    var NebulaScene = /** @class */ (function (_super) {
        __extends(NebulaScene, _super);
        function NebulaScene() {
            var _this = _super.call(this) || this;
            _this.nodes = [];
            _this.init();
            return _this;
        }
        NebulaScene.prototype.init = function () {
            var _this = this;
            //this.addBg();
            this.initUnityAssets();
            this.initNebulaPS();
            this.initStar();
            this.initCamera();
            this.addFg();
            this.light.removeSelf();
            //this.assets.addChild(this.star);
            this.star.addComponent(script.RotateSelfScript);
            this.nebulaPS.particleSystem.play();
            setTimeout(function () {
                _this.fg.playUIEffect();
                //this.bg.playUIEffect();
                _this.cameraScript.playZoomInAnimation();
                setTimeout(function () {
                    _this.cameraScript.enableTouch();
                }, 2200);
            }, 100);
        };
        NebulaScene.prototype.addBg = function () {
            this.bg = new view.BG();
            GlobalVar.bgLayer.addChild(this.bg);
        };
        NebulaScene.prototype.addFg = function () {
            this.fg = new view.FG();
            GlobalVar.uiLayer.addChild(this.fg);
        };
        NebulaScene.prototype.initUnityAssets = function () {
            this.assets = Laya.loader.getRes("res/LayaScene_Nebula/Nebula.lh");
            this.light = this.assets.getChildByName("Directional Light");
            this.addChild(this.assets);
        };
        NebulaScene.prototype.initStar = function () {
            this.star = this.assets.getChildByName("Star");
            this.setModelPureWhite(this.star);
        };
        NebulaScene.prototype.setModelPureWhite = function (node) {
            for (var i = 0; i < node.numChildren; i++) {
                var child = node.getChildAt(i);
                if (child instanceof Laya.MeshSprite3D) {
                    child.meshRender.material.albedoColor = new Laya.Vector4(1, 1, 1, 1);
                }
                else {
                    this.setModelPureWhite(child);
                }
            }
        };
        NebulaScene.prototype.initNebulaPS = function () {
            this.nebulaPS = this.assets.getChildByName("NebulaPS");
        };
        NebulaScene.prototype.initCamera = function () {
            this.camera = this.assets.getChildByName("Camera");
            this.cameraScript = this.camera.addComponent(script.CameraScript);
            GlobalVar.mainCamera = this.camera.getChildByName("rx").getChildByName("tz").getChildByName("Main Camera");
        };
        NebulaScene.prototype.initNodes = function () {
            for (var i = 0; i < 5; i++) {
                var node = new TouchNode(new Laya.Vector3(2 + 2 * i, 0, 0));
                node.on(Laya.Event.CHANGE, this, this.onNodeClick);
                GlobalVar.touchNodeLayer.addChild(node);
            }
        };
        NebulaScene.prototype.onNodeClick = function (pos) {
            this.cameraScript.moveTo(pos);
        };
        return NebulaScene;
    }(Laya.Scene));
    view.NebulaScene = NebulaScene;
})(view || (view = {}));
//# sourceMappingURL=NebulaScene.js.map