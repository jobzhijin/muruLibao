var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**Created by the LayaAirIDE*/
var view;
(function (view) {
    var FG = /** @class */ (function (_super) {
        __extends(FG, _super);
        function FG() {
            var _this = _super.call(this) || this;
            _this.width = Laya.stage.width;
            _this.height = Laya.stage.height;
            return _this;
        }
        FG.prototype.createChildren = function () {
            _super.prototype.createChildren.call(this);
            this.libao.on(Laya.Event.CLICK, this, this.openLibao);
            this.tfx.on(Laya.Event.CLICK, this, this.openTFX);
        };
        FG.prototype.doFade = function () {
            var _this = this;
            Laya.Tween.to(this.fade, { alpha: 0 }, 2000, Laya.Ease.linearNone, Laya.Handler.create(this, function () {
                _this.fade.removeSelf();
            }));
        };
        FG.prototype.playUIEffect = function () {
            this.logoAlpha1();
            this.tfxAlpha0();
            this.libaoFlowUp();
        };
        FG.prototype.logoAlpha1 = function () {
            //Laya.Tween.to(this.logo,{alpha:1},4000,Laya.Ease.linearNone);
        };
        FG.prototype.tfxAlpha0 = function () {
            var _this = this;
            Laya.Tween.to(this.tfxBg, { alpha: 0 }, 2000, Laya.Ease.linearNone, new laya.utils.Handler(this, function () {
                _this.tfxAlpha1();
            }));
        };
        FG.prototype.tfxAlpha1 = function () {
            var _this = this;
            Laya.Tween.to(this.tfxBg, { alpha: 1 }, 2000, Laya.Ease.linearNone, new laya.utils.Handler(this, function () {
                _this.tfxAlpha0();
            }));
        };
        FG.prototype.libaoFlowUp = function () {
            var _this = this;
            Laya.Tween.to(this.libao, { y: this.libao.y - 30 }, 3000, Laya.Ease.linearNone, new laya.utils.Handler(this, function () {
                _this.libaoFlowDown();
            }));
        };
        FG.prototype.libaoFlowDown = function () {
            var _this = this;
            Laya.Tween.to(this.libao, { y: this.libao.y + 30 }, 3000, Laya.Ease.linearNone, new laya.utils.Handler(this, function () {
                _this.libaoFlowUp();
            }));
        };
        FG.prototype.openLibao = function () {
            Laya.Browser.window.lingqu();
        };
        FG.prototype.openTFX = function () {
            Laya.Browser.window.dingzhi();
        };
        return FG;
    }(ui.FGUI));
    view.FG = FG;
})(view || (view = {}));
//# sourceMappingURL=FG.js.map