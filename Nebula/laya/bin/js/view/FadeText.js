var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name
*/
var view;
(function (view) {
    var FadeText = /** @class */ (function (_super) {
        __extends(FadeText, _super);
        function FadeText() {
            var _this = _super.call(this) || this;
            _this.txt = new Laya.Sprite();
            _this.addChild(_this.txt);
            _this.alpha = 0;
            return _this;
        }
        FadeText.prototype.show = function (name, parent, x, y, scale, hideTimeout) {
            var _this = this;
            if (scale === void 0) { scale = 1; }
            this._scale = scale;
            this.txt.loadImage(name, 0, 0, 0, 0, Laya.Handler.create(this, this.center, null, true));
            this.alpha1();
            parent.addChild(this);
            this.x = x;
            this.y = y;
            if (hideTimeout != 0 && hideTimeout != null) {
                setTimeout(function () {
                    _this.alpha0();
                }, hideTimeout);
            }
        };
        FadeText.prototype.hide = function () {
            this.alpha0();
        };
        FadeText.prototype.center = function () {
            this.txt.scale(this._scale, this._scale);
            this.txt.x = -this.txt.width * this._scale / 2;
            this.txt.y = -this.txt.height * this._scale / 2;
        };
        FadeText.prototype.alpha0 = function () {
            var _this = this;
            Laya.Tween.to(this, { alpha: 0 }, 1700, Laya.Ease.linearNone, Laya.Handler.create(this, function () {
                _this.removeSelf();
                _this.destroy(true);
            }, null, true));
        };
        FadeText.prototype.alpha1 = function () {
            Laya.Tween.to(this, { alpha: 1 }, 600, Laya.Ease.linearNone, Laya.Handler.create(this, function () {
            }, null, true));
        };
        return FadeText;
    }(Laya.Sprite));
    view.FadeText = FadeText;
})(view || (view = {}));
//# sourceMappingURL=FadeText.js.map