var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**Created by the LayaAirIDE*/
var view;
(function (view) {
    var BG = /** @class */ (function (_super) {
        __extends(BG, _super);
        function BG() {
            var _this = _super.call(this) || this;
            _this.width = Laya.stage.width;
            _this.height = Laya.stage.height;
            return _this;
        }
        BG.prototype.createChildren = function () {
            _super.prototype.createChildren.call(this);
        };
        BG.prototype.playUIEffect = function () {
            var _this = this;
            setInterval(function () {
                _this.createStarParticle();
            }, 150);
        };
        BG.prototype.scroll = function () {
            Laya.timer.frameLoop(1, this, this.scrollHandler);
        };
        BG.prototype.scrollHandler = function () {
            if (this.bg0.x < 0) {
                this.bg1.x = this.bg0.x + this.bg0.width;
            }
            if (this.bg1.x < 0) {
                this.bg0.x = this.bg1.x + this.bg1.width;
            }
            this.bg0.x -= 0.2;
            this.bg1.x -= 0.2;
        };
        BG.prototype.createStarParticle = function () {
            var _this = this;
            var idx = Math.ceil(Math.random() * 3);
            var s = new Laya.Sprite();
            s.loadImage("ui/s" + idx + ".png");
            s.alpha = 0;
            s.x = Math.random() * Laya.stage.width;
            s.y = Math.random() * Laya.stage.height;
            this.addChild(s);
            Laya.Tween.to(s, { alpha: 1 }, 200, Laya.Ease.linearNone, new laya.utils.Handler(this, function () {
                Laya.Tween.to(s, { alpha: 0 }, 100, Laya.Ease.linearNone, new laya.utils.Handler(_this, function () {
                    s.removeSelf();
                    s.destroy(true);
                }));
            }));
        };
        BG.prototype.fadeIn = function () {
            Laya.Tween.to(this, { alpha: 1 }, 1000, Laya.Ease.linearNone);
        };
        return BG;
    }(ui.BGUI));
    view.BG = BG;
})(view || (view = {}));
//# sourceMappingURL=BG.js.map