var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/*
* name;
*/
var BoomScene = /** @class */ (function (_super) {
    __extends(BoomScene, _super);
    function BoomScene() {
        var _this = _super.call(this) || this;
        _this.init();
        return _this;
    }
    BoomScene.prototype.init = function () {
        this.initUnityAssets();
    };
    BoomScene.prototype.initUnityAssets = function () {
        this.assets = Laya.loader.getRes("res/LayaScene_Boom/Boom.lh");
        this.addChild(this.assets);
        var skyBox = new Laya.SkyBox();
        var camera = this.assets.getChildByName("Main Camera");
        camera.clearFlag = Laya.BaseCamera.CLEARFLAG_SKY;
        camera.sky = skyBox;
        skyBox.textureCube = Laya.TextureCube.load("SkyCube.ltc");
    };
    BoomScene.prototype.clearSkybox = function () {
        this.assets.getChildByName("Main Camera").clearFlag = Laya.BaseCamera.CLEARFLAG_NONE;
    };
    BoomScene.prototype.dispose = function () {
        var _this = this;
        setTimeout(function () {
            _this.removeSelf();
            _this.destroy(true);
        }, 300);
    };
    return BoomScene;
}(Laya.Scene));
//# sourceMappingURL=BoomScene.js.map