var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var View = laya.ui.View;
var Dialog = laya.ui.Dialog;
var ui;
(function (ui) {
    var BGUI = /** @class */ (function (_super) {
        __extends(BGUI, _super);
        function BGUI() {
            return _super.call(this) || this;
        }
        BGUI.prototype.createChildren = function () {
            _super.prototype.createChildren.call(this);
            this.createView(ui.BGUI.uiView);
        };
        BGUI.uiView = { "type": "View", "props": { "width": 640, "height": 1136 }, "child": [{ "type": "Image", "props": { "var": "bg0", "skin": "ui/bg.jpg", "centerY": 0, "centerX": 0 } }, { "type": "Image", "props": { "y": -107, "x": -55, "var": "bg1", "skin": "ui/bg.jpg", "centerY": 0, "centerX": 0 } }] };
        return BGUI;
    }(View));
    ui.BGUI = BGUI;
})(ui || (ui = {}));
(function (ui) {
    var FGUI = /** @class */ (function (_super) {
        __extends(FGUI, _super);
        function FGUI() {
            return _super.call(this) || this;
        }
        FGUI.prototype.createChildren = function () {
            _super.prototype.createChildren.call(this);
            this.createView(ui.FGUI.uiView);
        };
        FGUI.uiView = { "type": "View", "props": { "width": 640, "height": 1136 }, "child": [{ "type": "Image", "props": { "var": "libao", "skin": "ui/Home-page_1_27.png", "scaleY": 0.8, "scaleX": 0.8, "right": 20, "bottom": 200 } }, { "type": "Image", "props": { "var": "tfxBg", "skin": "ui/tfx_bg.png", "scaleY": 0.8, "scaleX": 0.8, "centerX": 0, "bottom": 65 } }, { "type": "Image", "props": { "var": "tfx", "skin": "ui/tfx_fg.png", "scaleY": 0.8, "scaleX": 0.8, "centerX": 0, "bottom": 81 } }, { "type": "Sprite", "props": { "var": "fade" } }, { "type": "Image", "props": { "y": 341, "x": 243, "skin": "ui/Tex.png" } }, { "type": "Image", "props": { "y": 341, "x": 240, "skin": "ui/JIan.png" }, "compId": 9 }, { "type": "Image", "props": { "y": 341, "x": 390, "skin": "ui/JIan.png", "scaleX": -1 }, "compId": 10 }], "animations": [{ "nodes": [{ "target": 9, "keyframes": { "x": [{ "value": 233, "tweenMethod": "linearNone", "tween": true, "target": 9, "key": "x", "index": 0 }, { "value": 241, "tweenMethod": "linearNone", "tween": true, "target": 9, "key": "x", "index": 15 }, { "value": 233, "tweenMethod": "linearNone", "tween": true, "target": 9, "label": null, "key": "x", "index": 30 }] } }, { "target": 10, "keyframes": { "x": [{ "value": 398, "tweenMethod": "linearNone", "tween": true, "target": 10, "key": "x", "index": 0 }, { "value": 389, "tweenMethod": "linearNone", "tween": true, "target": 10, "key": "x", "index": 15 }, { "value": 398, "tweenMethod": "linearNone", "tween": true, "target": 10, "label": null, "key": "x", "index": 30 }] } }], "name": "ani1", "id": 1, "frameRate": 24, "action": 2 }] };
        return FGUI;
    }(View));
    ui.FGUI = FGUI;
})(ui || (ui = {}));
//# sourceMappingURL=layaUI.max.all.js.map