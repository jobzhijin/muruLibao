var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name
*/
var script;
(function (script) {
    var NodeScript = /** @class */ (function (_super) {
        __extends(NodeScript, _super);
        function NodeScript() {
            return _super.call(this) || this;
        }
        NodeScript.prototype._load = function (owner) {
            this.gameObject = owner;
        };
        //组件加载完成后初始化 相当于Unity中的Start
        NodeScript.prototype._start = function (state) {
            //console.log(this.gameObject.transform.forward.x,this.gameObject.transform.forward.y,this.gameObject.transform.forward.z)
        };
        //每帧触发 相当于Unity中的Update
        NodeScript.prototype._update = function (state) {
            //let faceToScreenQ = new Laya.Quaternion();
            //Laya.Quaternion.rotationLookAt(new Laya.Vector3(-GlobalVar.mainCamera.transform.forward.x,GlobalVar.mainCamera.transform.forward.y,GlobalVar.mainCamera.transform.forward.z),Laya.Vector3.Up,faceToScreenQ);
            //console.log(faceToScreenQ.x,faceToScreenQ.y,faceToScreenQ.z);
            //this.gameObject.transform.rotation = faceToScreenQ;
        };
        return NodeScript;
    }(Laya.Script));
    script.NodeScript = NodeScript;
})(script || (script = {}));
//# sourceMappingURL=NodeScript.js.map