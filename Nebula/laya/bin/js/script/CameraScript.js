var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name
*/
var script;
(function (script) {
    var CameraScript = /** @class */ (function (_super) {
        __extends(CameraScript, _super);
        function CameraScript() {
            var _this = _super.call(this) || this;
            _this.lastTouchX = 0;
            _this.lastTouchY = 0;
            _this.lastTouchScale = 0;
            _this.state = CameraState.Free;
            _this.NaviCounter = 0;
            _this.MaxNaviCounter = 120;
            _this.curOrien = 0;
            return _this;
        }
        CameraScript.prototype._load = function (owner) {
            this.gameObject = owner;
        };
        //组件加载完成后初始化 相当于Unity中的Start
        CameraScript.prototype._start = function (state) {
            this.rx = this.gameObject.getChildByName("rx");
            this.tz = this.rx.getChildByName("tz");
            this.mainCamera = this.tz.getChildByName("Main Camera");
        };
        //每帧触发 相当于Unity中的Update
        CameraScript.prototype._update = function (state) {
            if (this.state == CameraState.Lock) {
                this.event("OrienStopped");
                return;
            }
            else if (this.state == CameraState.Navi) {
                this.NaviCounter++;
                if (this.NaviCounter > this.MaxNaviCounter) {
                    this.state = CameraState.Free;
                }
                else {
                    var rot = new Laya.Vector3();
                    Laya.Vector3.lerp(Laya.Vector3.ZERO, this.endTran, 1 / this.MaxNaviCounter, rot);
                    this.gameObject.transform.rotate(new Laya.Vector3(0, rot.y, 0), true, false);
                    //this.rx.transform.rotate(new Laya.Vector3(rot.x,0,0),true,true);
                    //this.tz.transform.translate(new Laya.Vector3(0,0,rot.z),true);
                }
            }
            else {
                if (Math.abs(this.curOrien) > 5 && this.gameObject != null) {
                    this.gameObject.transform.rotate(new Laya.Vector3(0, this.curOrien * 0.002, 0), true, false);
                    this.event("OrienStarted");
                    return;
                }
            }
            this.gameObject.transform.rotate(new Laya.Vector3(0, 0.01, 0), true, false);
            this.event("OrienStopped");
        };
        CameraScript.prototype.enableTouch = function () {
            this.state = CameraState.Free;
            CameraScript.thisObj = this;
            Laya.stage.on(Laya.Event.MOUSE_DOWN, this, this.onMouseDown);
            Laya.stage.on(Laya.Event.MOUSE_UP, this, this.onMouseUp);
            Laya.Browser.window.addEventListener("deviceorientation", this.orientationHandler, false);
        };
        CameraScript.prototype.disableTouch = function () {
            this.state = CameraState.Lock;
            CameraScript.thisObj = null;
            this.curOrien = 0;
            Laya.stage.off(Laya.Event.MOUSE_DOWN, this, this.onMouseDown);
            Laya.stage.off(Laya.Event.MOUSE_UP, this, this.onMouseUp);
            Laya.stage.off(Laya.Event.MOUSE_MOVE, this, this.onMouseMove);
            Laya.Browser.window.removeEventListener("deviceorientation", this.orientationHandler, false);
        };
        CameraScript.prototype.orientationHandler = function (e) {
            var orientation = (Laya.Browser.window.orientation || 0);
            if (Laya.stage.canvasRotation) {
                if (Laya.stage.screenMode == Laya.Stage.SCREEN_HORIZONTAL)
                    orientation += 90;
                else if (Laya.stage.screenMode == Laya.Stage.SCREEN_VERTICAL)
                    orientation -= 90;
                //Laya.Browser.window.orientation = orientation;
            }
            if (e.gamma != null) {
                //txt.text=e.alpha+"  "+e.beta+"  "+e.gamma;
                CameraScript.thisObj.curOrien = e.gamma;
            }
        };
        CameraScript.prototype.onMouseDown = function (ev) {
            if (this.state == CameraState.Navi)
                return;
            var touches = ev.touches;
            if (touches && touches.length == 2) {
                this.lastTouchScale = this.getPointsDistance(touches[0], touches[1]);
            }
            else if (touches && touches.length > 2) {
            }
            else {
                this.touchDownPosX = this.lastTouchX = ev.stageX;
                this.touchDownPosY = this.lastTouchY = ev.stageY;
                this.touchDownTime = Date.now();
                Laya.stage.on(Laya.Event.MOUSE_MOVE, this, this.onMouseMove);
            }
        };
        CameraScript.prototype.onMouseUp = function (ev) {
            var touches = ev.touches;
            if (touches && touches.length == 2) {
            }
            else if (touches && touches.length > 2) {
            }
            else {
                Laya.stage.off(Laya.Event.MOUSE_MOVE, this, this.onMouseMove);
                if (Date.now() - this.touchDownTime < 300) {
                    if (ev.stageX - this.touchDownPosX < -150) {
                        this.event(Laya.Event.DRAG_END, -1);
                    }
                    else if (ev.stageX - this.touchDownPosX > 150) {
                        this.event(Laya.Event.DRAG_END, 1);
                    }
                }
            }
        };
        CameraScript.prototype.onMouseMove = function (ev) {
            var touches = ev.touches;
            if (touches && touches.length == 2) {
                var newTouchScale = this.getPointsDistance(touches[0], touches[1]);
                var moveZ = 0.02 * (newTouchScale - this.lastTouchScale);
                var newZ = this.tz.transform.localPosition.z + moveZ;
                if (newZ >= -35 && newZ <= -1) {
                    this.tz.transform.localPosition = new Laya.Vector3(0, 0, newZ);
                }
                this.lastTouchScale = newTouchScale;
            }
            else if (touches && touches.length > 2) {
            }
            else {
                var rotY = 0.01 * (ev.stageX - this.lastTouchX);
                var rotX = 0.01 * (ev.stageY - this.lastTouchY);
                this.gameObject.transform.rotate(new Laya.Vector3(0, -rotY * 3, 0), true, false);
                var newRX = this.rx.transform.localRotationEuler.x + rotX;
                if (newRX >= 10 && newRX <= 60) {
                    //this.rx.transform.rotate(new Laya.Vector3(rotX,0,0),true,false);		
                }
                this.lastTouchX = ev.stageX;
                this.lastTouchY = ev.stageY;
            }
        };
        CameraScript.prototype.getPointsDistance = function (a, b) {
            var dx = a.stageX - b.stageX;
            var dy = a.stageY - b.stageY;
            return Math.sqrt(dx * dx + dy * dy);
        };
        CameraScript.prototype.moveTo = function (pos, from, dir) {
            if (this.state == CameraState.Free) {
                this.state = CameraState.Navi;
                this.NaviCounter = 0;
                var targeQ = new Laya.Quaternion();
                Laya.Quaternion.lookAt(Laya.Vector3.ZERO, pos, Laya.Vector3.Up, targeQ);
                var targetAngle = new Laya.Vector3();
                targeQ.getYawPitchRoll(targetAngle);
                //pitch 绕x轴旋转
                var cameraXRot = this.rx.transform.localRotationEuler.x;
                //yaw 绕y轴旋转 通过节点坐标计算出的RotationEuler.y和相机在该节点位置对应的RotationEuler.y是相反的，所以这里要取负统一到节点所在坐标系。
                var cameraYRot = -this.gameObject.transform.localRotationEuler.y;
                // 计算相机缩放
                var zTranslate = Math.abs(this.tz.transform.localPosition.z) - Laya.Vector3.scalarLength(pos);
                this.endTran = new Laya.Vector3(targetAngle.z - cameraXRot + 0.4, cameraYRot - targetAngle.x * 180 / Math.PI, zTranslate);
                console.log(this.endTran);
            }
        };
        CameraScript.prototype.playZoomInAnimation = function () {
            var ani = this.gameObject.getComponentByType(Laya.Animator);
            ani.play("CamAnimation");
        };
        return CameraScript;
    }(Laya.Script));
    script.CameraScript = CameraScript;
    var CameraState;
    (function (CameraState) {
        CameraState[CameraState["Free"] = 0] = "Free";
        CameraState[CameraState["Lock"] = 1] = "Lock";
        CameraState[CameraState["Navi"] = 2] = "Navi";
    })(CameraState || (CameraState = {}));
})(script || (script = {}));
//# sourceMappingURL=CameraScript.js.map