var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name
*/
var script;
(function (script) {
    var PersonScript = /** @class */ (function (_super) {
        __extends(PersonScript, _super);
        function PersonScript() {
            var _this = _super.call(this) || this;
            _this.zOrder = 0;
            return _this;
        }
        PersonScript.prototype._load = function (owner) {
            this.gameObject = owner;
        };
        //组件加载完成后初始化 相当于Unity中的Start
        PersonScript.prototype._start = function (state) {
            var _this = this;
            this.title = new Laya.Image();
            var brandName = Laya.Browser.window.brandName;
            this.title.loadImage("ui/" + brandName + "/p" + this.index + ".png", 0, 0, 0, 0, Laya.Handler.create(this, function () {
                //this.title.loadImage("ui/p"+this.index+".png",0,0,0,0,Laya.Handler.create(this,()=>{
                //this.title.scale(0.7,0.7);
                _this.title.pivotY = _this.title.height;
            }, null, true));
            GlobalVar.uiLayer.addChild(this.title);
            //console.log(this.gameObject.transform.forward.x,this.gameObject.transform.forward.y,this.gameObject.transform.forward.z)
        };
        //每帧触发 相当于Unity中的Update
        PersonScript.prototype._update = function (state) {
            var screenPos = new Laya.Vector3();
            GlobalVar.mainCamera.worldToViewportPoint(this.gameObject.transform.position, screenPos);
            this.title.x = screenPos.x;
            this.title.y = screenPos.y;
            this.zOrder = screenPos.z;
            if (screenPos.z > 0.98) {
                this.title.alpha = screenPos.z * 0.5;
            }
            else {
                this.title.alpha = 1;
            }
            var scaleValue = (1 - screenPos.z) / 0.04 + 0.2;
            if (scaleValue > 0.7)
                scaleValue = 0.7;
            this.title.scale(scaleValue, scaleValue);
        };
        return PersonScript;
    }(Laya.Script));
    script.PersonScript = PersonScript;
})(script || (script = {}));
//# sourceMappingURL=PersonScript.js.map