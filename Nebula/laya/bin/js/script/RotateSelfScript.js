var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name
*/
var script;
(function (script) {
    var RotateSelfScript = /** @class */ (function (_super) {
        __extends(RotateSelfScript, _super);
        function RotateSelfScript() {
            var _this = _super.call(this) || this;
            _this.state = RotateState.Free;
            return _this;
        }
        RotateSelfScript.prototype._load = function (owner) {
            this.gameObject = owner;
        };
        //组件加载完成后初始化 相当于Unity中的Start
        RotateSelfScript.prototype._start = function (state) {
        };
        //每帧触发 相当于Unity中的Update
        RotateSelfScript.prototype._update = function (state) {
            if (this.state == RotateState.Free)
                this.gameObject.transform.rotate(new Laya.Vector3(0, -0.0002, 0), true);
        };
        return RotateSelfScript;
    }(Laya.Script));
    script.RotateSelfScript = RotateSelfScript;
    var RotateState;
    (function (RotateState) {
        RotateState[RotateState["Free"] = 0] = "Free";
        RotateState[RotateState["Lock"] = 1] = "Lock";
    })(RotateState = script.RotateState || (script.RotateState = {}));
})(script || (script = {}));
//# sourceMappingURL=RotateSelfScript.js.map