var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
* name Mac
*/
var script;
(function (script) {
    var Vector3 = Laya.Vector3;
    var Event = Laya.Event;
    var FreeCameraScript = /** @class */ (function (_super) {
        __extends(FreeCameraScript, _super);
        function FreeCameraScript() {
            var _this = _super.call(this) || this;
            _this.moveSpeed = 0.01;
            _this.rotateSpeed = 0.08;
            _this.lastMousePosX = 0.0;
            _this.lastMousePosY = 0.0;
            _this.mouseRightKeyDown = false;
            _this.currentKey = -1;
            _this.currentRMouse = 0;
            return _this;
        }
        FreeCameraScript.prototype._load = function (owner) {
            this.gameObject = owner;
        };
        FreeCameraScript.prototype._start = function (state) {
            Laya.stage.on(Event.KEY_DOWN, this, this.onKeyDown);
            Laya.stage.on(Event.KEY_UP, this, this.onKeyUp);
            Laya.stage.on(Event.RIGHT_MOUSE_DOWN, this, this.onRMouseDown);
            Laya.stage.on(Event.RIGHT_MOUSE_UP, this, this.onRMouseUp);
        };
        FreeCameraScript.prototype.onKeyDown = function (ev) {
            this.currentKey = ev.keyCode;
        };
        FreeCameraScript.prototype.onKeyUp = function (ev) {
            this.currentKey = -1;
        };
        FreeCameraScript.prototype.onRMouseDown = function () {
            this.currentRMouse = 1;
        };
        FreeCameraScript.prototype.onRMouseUp = function () {
            this.currentRMouse = 0;
        };
        FreeCameraScript.prototype._update = function (state) {
            if (this.currentRMouse == 1) {
                if (this.mouseRightKeyDown == false) {
                    this.mouseRightKeyDown = true;
                    this.lastMousePosX = Laya.stage.mouseX;
                    this.lastMousePosY = Laya.stage.mouseY;
                }
                else {
                    var fDeltaX = Laya.stage.mouseX - this.lastMousePosX;
                    var fDeltaY = Laya.stage.mouseY - this.lastMousePosY;
                    this.lastMousePosX = Laya.stage.mouseX;
                    this.lastMousePosY = Laya.stage.mouseY;
                    var kNewEuler = this.gameObject.transform.localRotationEuler;
                    kNewEuler.x -= (fDeltaY * this.rotateSpeed);
                    kNewEuler.y -= (fDeltaX * this.rotateSpeed);
                    this.gameObject.transform.localRotationEuler = kNewEuler;
                }
            }
            else if (this.currentRMouse == 0) {
                if (this.mouseRightKeyDown == true) {
                    this.mouseRightKeyDown = false;
                    this.lastMousePosX = 0;
                    this.lastMousePosY = 0;
                }
            }
            var fMoveDeltaX = 0.0;
            var fMoveDeltaY = 0.0;
            var fMoveDeltaZ = 0.0;
            var fDeltaTime = Laya.timer.delta;
            if (this.currentKey == Laya.Keyboard.A) {
                fMoveDeltaX -= this.moveSpeed * fDeltaTime;
            }
            if (this.currentKey == Laya.Keyboard.D) {
                fMoveDeltaX += this.moveSpeed * fDeltaTime;
            }
            if (this.currentKey == Laya.Keyboard.W) {
                fMoveDeltaY += this.moveSpeed * fDeltaTime;
            }
            if (this.currentKey == Laya.Keyboard.S) {
                fMoveDeltaY -= this.moveSpeed * fDeltaTime;
            }
            if (this.currentKey == Laya.Keyboard.NUMBER_1) {
                fMoveDeltaZ += this.moveSpeed * fDeltaTime;
            }
            if (this.currentKey == Laya.Keyboard.NUMBER_2) {
                fMoveDeltaZ -= this.moveSpeed * fDeltaTime;
            }
            if (fMoveDeltaX != 0.0 || fMoveDeltaY != 0.0 || fMoveDeltaZ != 0.0) {
                var kForward = this.gameObject.transform.forward;
                var kRight = new Vector3();
                var kUp = new Vector3();
                Vector3.cross(FreeCameraScript.kUpDirection, kForward, kRight);
                Vector3.cross(kRight, kForward, kUp);
                var kNewPos = this.gameObject.transform.position;
                var temp = new Vector3();
                Vector3.scale(kRight, fMoveDeltaX, temp);
                Vector3.subtract(kNewPos, temp, kNewPos);
                Vector3.scale(kUp, fMoveDeltaY, temp);
                Vector3.subtract(kNewPos, temp, kNewPos);
                Vector3.scale(kForward, fMoveDeltaZ, temp);
                Vector3.add(kNewPos, temp, kNewPos);
                this.gameObject.transform.position = kNewPos;
            }
        };
        FreeCameraScript.kUpDirection = new Vector3(0.0, 1.0, 0.0);
        return FreeCameraScript;
    }(Laya.Script));
    script.FreeCameraScript = FreeCameraScript;
})(script || (script = {}));
//# sourceMappingURL=FreeCameraScript.js.map